theory les2b
(* datatypes and examples for APSEC 2016 paper*)

imports les2

begin
datatype eventType2 = e0 | e1 | e2 | e5 | e4 | e2' | e3' | e3 | e4' | e5' | e0' | e1' 
(*| e8 | e9 | e10 | e11 | e12 | e0' | e1' | e4' | e5' *)

abbreviation "Ca1 == {(e0, e2),(e1, e3),(e2,e3), (e2,e4), (e3,e5)}"
abbreviation "Ca2 == {(e0, e2'),(e1, e3'),(e2',e3'), (e2',e4), (e3',e5)}"
abbreviation "Ca12=={(e0, e2),(e1, e3), (e2,e4), (e0, e2'), (e1, e3'), (e2',e3'), (e2,e3), (e3,e5), (e2',e4), (e3',e5)}"
abbreviation "wantedResult==Ca1 \<union> Ca2"


(* nitpick_params [sat_solver = MiniSat_JNI, max_threads = 1] *)

(*
lemma shows "\<not> (EX Ca. (events Ca= events Ca1 \<union> events Ca2 & Ca \<supseteq> Ca1 \<union> Ca2 & isLes Ca {}  
))"  nitpick [card eventType2=1-20, max_potential=0] 
*)

(*
lemma shows "\<not> (EX Ca Co. (events Ca= events Ca1 \<union> events Ca2 & Ca \<supseteq> Ca1 \<union> Ca2 & isLes Ca Co &
{(e2,e2')} \<subseteq> Co
))"  nitpick [card eventType2=1-20, max_potential=0]
*)
definition [simp]: "XC1=Domain Ca1"
abbreviation "symmDiff A B == A - B \<union> (B-A)"

abbreviation "composedCa == {(e0, e0), (e0, e2), (e0, e3), (e0, e4), (e0, e5), (e0, e2'), (e0, e3'), (e1, e1),
          (e1, e3), (e1, e5), (e1, e3'), (e2, e2), (e2, e3), (e2, e4), (e2, e5), (e3, e3),
          (e3, e5), (e4, e4), (e5, e5), (e2', e4), (e2', e5), (e2', e2'), (e2', e3'),
          (e3', e5), (e3', e3')}"
abbreviation "composedConcurrency == concurrency composedCa {}"
(* value "composedConcurrency - Union { {x} \<times> successors composedCa {y}| x y. (x,y) \<in> composedConcurrency} - Union {successors composedCa {x} \<times> {y} |x y. (x,y) \<in> composedConcurrency}" *)

abbreviation "newConcurrencies == (concurrency composedCa {} - concurrency Ca1 {} - concurrency Ca2 {})"
abbreviation "divergingTriangles == 
((% (x,y).({x,y}, predecessors (order2strictCover' composedCa) {x} \<inter> predecessors (order2strictCover' composedCa) {y})) ` newConcurrencies)"
abbreviation "convergingTriangles == ((% (x,y).({x,y}, successors (order2strictCover' composedCa) {x} \<inter> successors (order2strictCover' composedCa) {y})) ` newConcurrencies)"
abbreviation "convergingTrianglesNonDegenerate == convergingTriangles - (Domain convergingTriangles \<times> {{}})"
abbreviation "divergingTrianglesNonDegenerate == divergingTriangles - (Domain divergingTriangles \<times> {{}})"
abbreviation "freshEventGenerator == {(e0, e0'), (e1,e1'), (e4, e4'), (e5, e5')}"
(*value "divergingTrianglesNonDegenerate"*)

definition "extendedComposedCa = composedCa \<union> 
Union ((% (X, Z). Z \<times> freshEventGenerator``Z \<union> (freshEventGenerator``Z) \<times> X)`divergingTrianglesNonDegenerate)
\<union> (Union ((% (X, Z). X \<times> freshEventGenerator``Z \<union> (freshEventGenerator``Z) \<times> Z)`convergingTrianglesNonDegenerate))"

abbreviation "mbc01 == {(0::int, {x::int. x<5})}"

abbreviation "mbc02 == {(0::int, {x::int. x>15})}"

(*
lemma assumes 
"ca1 0 2"
"ca1 1 3"
"ca1 2 3"
"ca1 2 4"
"ca1 3 5"

"ca2 0 12"
"ca2 1 13"
"ca2 12 13"
"ca2 12 4"
"ca2 (13::int) (5::int)"

"\<forall> e e'. ca e e' \<longrightarrow> (ca1 e e' \<or> ca2 e e')"
"\<forall> e. (ca1 e e \<or> ca2 e e) \<longleftrightarrow> ca e e"
"co 2 12"
"\<forall> e e'. co1 e e' = False"
"\<forall> e e'. co2 e e' = False"
"IsLes ca1 co1"
"IsLes ca2 co2"
"IsLes ca co"
(*"\<forall> ca'. (ca ee ee' \<noteq> ca' ee ee' & (\<forall> e e'. e \<noteq> ee & e' \<noteq> ee' \<longrightarrow> ca e e' = ca' e e') \<longrightarrow> \<not> (IsLes ca' co))"*)
shows False
sorry


lemma assumes "(\<forall> x y. set2pred divergingTrianglesNonDegenerate x y \<longrightarrow> (\<exists> z. {z} \<times> x \<subseteq> Ca & y \<times> {z} \<subseteq> Ca ))"
"(\<forall> x y. set2pred convergingTrianglesNonDegenerate x y \<longrightarrow> (\<exists> z. x \<times> {z} \<subseteq> Ca & {z} \<times> y \<subseteq> Ca))"
(*"(\<forall>(x,y)\<in>convergingTrianglesNonDegenerate. x \<times> f (x,y) \<subseteq> Ca & f (x,y) \<times> y\<subseteq> Ca)"*)
(*"(inj_on f (divergingTrianglesNonDegenerate \<union> convergingTrianglesNonDegenerate))"*)
"composedCa \<subseteq> Ca"
"(isLes Ca {})"
 shows "Ca=composedCa"
nitpick[card eventType2=1-20]
sledgehammer run [provers=z3, minimize=false, overlord=true, timeout=1] (assms)
*)

(*
lemma "\<not> ((isLes Ca {} & composedCa \<subseteq> Ca) & 
(\<forall> e e'. (e, e') \<in> concurrency composedCa {} \<longrightarrow> 
  (EX en1 en2. 
  en1 \<notin> events composedCa & en2 \<notin> events composedCa & en1 \<noteq> en2 &
  (en1, e) \<in> Ca & (en1, e')\<in>Ca & (e,en2) \<in> Ca & (e', en2)\<in>Ca & 
    (\<forall> e01. (e01,e) \<in> composedCa & (e01,e')\<in>composedCa \<longrightarrow> (e01,en1) \<in> Ca) & 
    (\<forall> e02. (e,e02) \<in> composedCa & (e',e02)\<in>composedCa \<longrightarrow> (en2,e02) \<in> Ca) 
  )
)
)
" nitpick[card eventType2=8-50, max_potential=0]
*)

(*
abbreviation "testCa == 
 {(e0, e0), (e0, e1), (e0, e2), (e0, e3), (e0, e4), (e0, e5), (e0, e6), (e0, e7), (e0, e8), (e0, e10),
          (e0, e11), (e0, e12), (e0, e2'), (e0, e3'), (e1, e1), (e1, e2), (e1, e3), (e1, e4), (e1, e5), (e1, e7),
          (e1, e8), (e1, e10), (e1, e11), (e1, e12), (e1, e3'), (e2, e2), (e2, e3), (e2, e4), (e2, e5), (e2, e8),
          (e2, e10), (e2, e11), (e2, e12), (e3, e3), (e3, e5), (e3, e10), (e3, e12), (e4, e4), (e4, e5), (e4, e10),
          (e4, e11), (e4, e12), (e5, e5), (e5, e12), (e6, e1), (e6, e2), (e6, e3), (e6, e4), (e6, e5), (e6, e6),
          (e6, e7), (e6, e8), (e6, e10), (e6, e11), (e6, e12), (e6, e2'), (e6, e3'), (e7, e2), (e7, e3), (e7, e4),
          (e7, e5), (e7, e7), (e7, e8), (e7, e10), (e7, e11), (e7, e12), (e7, e3'), (e8, e3), (e8, e4), (e8, e5),
          (e8, e8), (e8, e10), (e8, e11), (e8, e12), (e9, e0), (e9, e1), (e9, e2), (e9, e3), (e9, e4), (e9, e5),
          (e9, e6), (e9, e7), (e9, e8), (e9, e9), (e9, e10), (e9, e11), (e9, e12), (e9, e2'), (e9, e3'), (e10, e5),
          (e10, e10), (e10, e12), (e11, e11), (e12, e12), (e2', e2), (e2', e3), (e2', e4), (e2', e5), (e2', e7),
          (e2', e8), (e2', e10), (e2', e11), (e2', e12), (e2', e2'), (e2', e3'), (e3', e3), (e3', e4), (e3', e5),
          (e3', e8), (e3', e10), (e3', e11), (e3', e12), (e3', e3')}
"
value "order2strictCover' testCa"
*)

(*
lemma assumes 
"ca3 e0 e2" 
"ca3 e1 e3" 
"ca3 e2 e3" 
"ca3 e2 e4" 
"ca3 e3 e5" 
"ca6 e0 e2'" 
"ca6 e1 e3'" 
"ca6 e2' e3'" 
"ca6 e2' e4" 
"ca6 e3' e5"
"\<forall> e ee. \<not> co e ee"
"IsLes ca3 co"
"IsLes ca6 co"
"\<forall> e ee. ca3 e ee \<longrightarrow> ca e ee & ca6 e ee \<longrightarrow> ca e ee"
"IsLes ca co"
shows "False"
sorry
*)































(*
lemma assumes "Ca1 \<union> Ca2 - C \<subseteq> Ca3" "card C\<le>2"
"(e2, e2') \<in> Co3" "(e2, e3) \<in> Ca3" "(e2', e3') \<in> Ca3"
shows "\<not> (isLes Ca3 Co3)" nitpick [card eventType2=8-14, timeout=40, max_potential=0]
*)

abbreviation "
    Ca3 == {(e0, e0), (e0, e2), (e0, e3), (e0, e4), (e0, e5), (e0, e2'), (e0, e3'), (e1, e1), (e1, e3), (e1, e5), (e1, e3'), (e2, e2), (e2, e3),
           (e3, e3), (e4, e4), (e5, e5), (e2', e4), (e2', e5), (e2', e2'), (e2', e3'), (e3', e5), (e3', e3')}"
abbreviation "Co3 == {(e2, e4), (e2, e5), (e2, e2'), (e2, e3'), (e3, e4), (e3, e5), (e3, e2'), (e3, e3'), (e4, e2), (e4, e3), (e5, e2), (e5, e3), (e2', e2),
           (e2', e3), (e3', e2), (e3', e3)}"


abbreviation "FinalCa == addEventToCausality (addEventToCausality Ca3 e2 e4') e3 e5'"
abbreviation "FinalCo == addEventToConflict (addEventToConflict Co3 e2 e4') e3 e5'"
(*value "order2strictCover' (addEventToCausality Ca3 e2 e4')"
value "order2strictCover' Ca3"*)
datatype eventType3 = eee0 | eee1 | eee2 
datatype eventType4 = eee0' | eee1' | eee2'

(*
lemma fixes f1 f2 :: "_ => int => int => bool"
assumes 
"f1 eee0 = (\<lambda> x0 x1. x0 < 5 & (x0 < x1))"
"f1 eee1 = (\<lambda> x0 x1. x0 > x1)"
"f1 eee2 = (\<lambda> x0 x1. x0 + x1 > 5)"
"f2 eee0' = (\<lambda> x0 x1. x0 < 3 & (x0 < x1))"
"f2 eee1' = (\<lambda> x0 x1. x0 + 1 > x1)"
"f2 eee2' = (\<lambda> x0 x1. x0 + x1 < 11)"
"\<forall> e e'.
(((\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1)) 
& (g e e' = True)) 
 \<or> ((\<not> (\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1))) & (g e e' = False)) 
)" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms)
*)
(*
lemma fixes f1 f2 :: "int => int => int => bool" 
assumes 
"f1 (map1 eee0) = (\<lambda> x0 x1. x0 < 5 & (x0 < x1))"
"f1 (map1 eee1) = (\<lambda> x0 x1. x0 > x1)"
"f1 (map1 eee2) = (\<lambda> x0 x1. x0 + x1 > 5)"
"f2 (map2 eee0') = (\<lambda> x0 x1. x0 < 3 & (x0 < x1))"
"f2 (map2 eee1') = (\<lambda> x0 x1. x0 + 1 > x1)"
"f2 (map2 eee2') = (\<lambda> x0 x1. x0 + x1 < 11)"
"\<forall> e e'. 
(e \<ge> 0 & e \<le> 2 & e' \<ge> 0 & e' \<le> 2) \<longrightarrow>  
(((\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1)) 
& (g e e' = True)) 
 \<or> ((\<not> (\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1))) & (g e e' = False)) 
)" 
"\<forall> e e'. (e \<ge> 0 & e \<le> 2 & e' \<ge> 0 & e' \<le> 2 & e \<noteq> e') \<longrightarrow> (map11 e \<noteq> map11 e' & map22 e \<noteq> map22 e'
& map1 (map11 e)=e & map2 (map22 e)=e)"
"\<forall> E. map1 E \<le>2 & map1 E \<ge>0"
"\<forall> E. map2 E \<le>2 & map2 E \<ge>0"
"\<forall> E E'. h E E' = g (map1 E) (map2 E')" 
shows False 
sledgehammer run [provers=z3, minimize=false, timeout=1, overlord=true] (assms)  
sorry
*)

(* APSEC
lemma fixes f1 f2 :: "int => int => int => bool" fixes map1 :: "_ => eventType3" 
fixes map2::"_=> eventType4"
assumes 
"f1 0 = (\<lambda> x0 x1. x0 < 5 & (x0 < x1))"
"f1 1 = (\<lambda> x0 x1. x0 > x1)"
"f1 2 = (\<lambda> x0 x1. x0 + x1 > 5)"
"f2 0 = (\<lambda> x0 x1. x0 < 3 & (x0 < x1))"
"f2 1 = (\<lambda> x0 x1. x0 + 1 > x1)"
"f2 2 = (\<lambda> x0 x1. x0 + x1 < 11)"
"\<forall> e e'. 
(e \<ge> 0 & e \<le> 2 & e' \<ge> 0 & e' \<le> 2) \<longrightarrow>  
(((\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1)) 
& (g e e' = True)) 
 \<or> ((\<not> (\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1))) & (g e e' = False)) 
)" 
"\<forall> e e'. (e \<ge> 0 & e \<le> 2 & e' \<ge> 0 & e' \<le> 2 & e \<noteq> e') \<longrightarrow> (map1 e \<noteq> map1 e' & map2 e \<noteq> map2 e'
& map11 (map1 e)=e & map22 (map2 e)=e)"
"\<forall> E. map11 E \<le>2 & map11 E \<ge>0"
"\<forall> E. map22 E \<le>2 & map22 E \<ge>0"
"\<forall> E E'. h E E' = g (map11 E) (map22 E')" 
shows False 
 (*sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms) *) 
sorry
*)

(*
lemma fixes f1 f2 :: "int => int => int => bool"
assumes 
"f1 0 = (\<lambda> x0 x1. x0 < 5 & (x0 < x1))"
"f1 1 = (\<lambda> x0 x1. x0 > x1)"
"f1 2 = (\<lambda> x0 x1. x0 + x1 > 5)"
"f2 0 = (\<lambda> x0 x1. x0 < 3 & (x0 < x1))"
"f2 1 = (\<lambda> x0 x1. x0 + 1 > x1)"
"f2 2 = (\<lambda> x0 x1. x0 + x1 < 11)"
"\<forall> e e'. 
(e \<ge> 0 & e \<le> 2 & e' \<ge> 0 & e' \<le> 2) \<longrightarrow>  
(((\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1)) 
& (g e e' = True)) 
 \<or> ((\<not> (\<exists> x0 x1. ((f1 e) x0 x1 & (f2 e') x0 x1))) & (g e e' = False)) 
)" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms)
*)

(* lemma 
(* fixes f1 :: "int => (int => bool)" *)
assumes 
"f1 (0::int) = (% x. x (0::int) <(5::int) & (x 0 < x 1))"
"f1 (1::int) = (% x. x 0 > x 1)"
"f2 (0::int) = (% x. x 0 <(5::int) & (x 0 < x 1))"
"f2 (1::int) = (% x. x 0 < x 1)"
"\<forall> e1 e2. (e1 \<ge> 0 & e1 \<le>1 & e2 \<ge> 0 & e2 \<le> 1) \<longrightarrow> (((EX x. ((f1 e1) x & (f2 e2) x)) 
& (g e1 e2 = True)) 
 \<or> ((\<not> (EX x. ((f1 e1) x & (f2 e2) x))) & (g e1 e2 = False)) 
)" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms)
*)

(*
lemma 
(* fixes f1 :: "int => (int => bool)" *) 
assumes 
"f (1::int) (1::int) = (%x. x<(5::int))"
"f 2 1 = (%x. x>10)" 
"f 1 2 = (%x. x>3)"
"f 2 2 = (%x. x>7)"
"f 1 3 = (%x. x<3 )"
"f 2 3 = (\<lambda>x. x<7)"
"f 1 0 = (\<lambda>x. x<3)"
"f 2 0 = (\<lambda>x. x>8)"
" \<forall> esIndex1 esIndex2 variableIndex. 
(esIndex1 \<le>2 & esIndex1 \<ge>1 & esIndex2 \<le>2 & esIndex2 \<ge>1) \<longrightarrow>
(EX x. (((ander (f esIndex1) (f esIndex2)) variableIndex x 
& (g variableIndex = True)
)) 
 \<or> ((\<not> (EX x. ((ander (f esIndex1) (f esIndex2)) variableIndex x))) & (g variableIndex = False)) 
)" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms)
*)

(*
lemma fixes f1 :: "int => (int => bool)" assumes 
"f1 1 = (%x. x<(5::int))"
"f2 1 = (%x. x>10)" 
"f1 2 = (%x. x>3)"
"f2 2 = (%x. x>7)"
"f1 3 = (%x. x<3)"
"f2 3 = (\<lambda>x. x<7)"
"f1 0 = (\<lambda>x. x<3)"
"f2 0 = (\<lambda>x. x>8)"
" \<forall> e. (EX x. (((ander f1 f2) e x 
& (g e = True)
)) 
 \<or> ((\<not> (EX x. ((ander f1 f2) e x))) & (g e = False)) 
)" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms)
*)

(*
lemma fixes f :: "mbc => (int => bool)" assumes 
"f a = (%x. x<(5::int) & x>10 )"
"f b = (%x. x>3 & x>7)"
"f c = (%x. x<3 & x<7)"
" \<forall> e. (EX x. ((f e x 
& (g e = True)
)) 
 \<or> ((\<not> (EX x. (f e x))) & (g e = False)) 
)" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms)
*)
(*
value "order2strictCover' FinalCa"
value "FinalCo"
*)

end
